package sk.topo.simplenote.viewmodel

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.Observer

/**
 * Project: SimpleNote
 * Created by porac
 * on 14-Feb-19.
 */
class TestObserver<T> : Observer<T> {

    val observedValues = mutableListOf<T?>()

    override fun onChanged(value: T?) {
        observedValues.add(value)
    }
}
fun <T> LiveData<T>.testObserver() = TestObserver<T>().also {
    observeForever(it)
}