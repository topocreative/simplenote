package sk.topo.simplenote.viewmodel

import android.arch.core.executor.testing.InstantTaskExecutorRule
import com.google.common.truth.Truth
import io.reactivex.disposables.CompositeDisposable
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.junit.MockitoJUnit
import sk.topo.simplenote.api.ApiHandler
import sk.topo.simplenote.api.model.NoteModel
import sk.topo.simplenote.repository.NotesRepository

/**
 * Project: SimpleNote
 * Created by porac
 * on 14-Feb-19.
 */
class NotesViewModelTest {


    @get:Rule
    val mockitoRule = MockitoJUnit.rule()

    @get:Rule
    val taskExecutorRule = InstantTaskExecutorRule()

    @get:Rule
    val rxSchedulerRule = RxSchedulerRule()

    lateinit var repository: NotesRepository

    lateinit var classUnderTest: NotesViewModel
    @Test
    fun testInitialState() {
        val liveDataUnderTest = classUnderTest.notesList.testObserver()

        Truth.assert_()
            .that(liveDataUnderTest.observedValues)
            .isEqualTo(emptyList<NoteModel>())
    }

    @Test
    fun testLoadNotes() {
        val liveDataUnderTest = classUnderTest.notesList.testObserver()
        classUnderTest.getNotes()
        Truth.assert_()
            .that(liveDataUnderTest.observedValues)
            .isNotEmpty()
    }



    @Before
    fun setUp() {
        ApiHandler.init()
        repository = NotesRepository()
        classUnderTest = NotesViewModel(repository, CompositeDisposable())
    }


}