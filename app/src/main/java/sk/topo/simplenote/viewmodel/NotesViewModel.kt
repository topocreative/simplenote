package sk.topo.simplenote.viewmodel

import android.arch.lifecycle.ViewModel
import io.reactivex.disposables.CompositeDisposable
import sk.topo.simplenote.api.toLiveData
import sk.topo.simplenote.repository.NotesRepository

/**
 * Project: SimpleNote
 * Created by porac
 * on 12-Feb-19.
 */
class NotesViewModel(private val repository: NotesRepository, private val compositeDisposable: CompositeDisposable) : ViewModel(){

    val notesList = repository.notesList.toLiveData(compositeDisposable)
    val singleNote = repository.singleNote.toLiveData(compositeDisposable)
    val removingCallback = repository.removingCallback.toLiveData(compositeDisposable)

    fun getNotes() {
        repository.getNotes()
    }

    fun addNote(title: String) {
        repository.newNote(title)
    }

    fun updateNote(id: Int, title: String) {
        repository.updateNote(id, title)
    }

    fun deleteNote(id: Int) {
        repository.deleteNote(id)
    }

    override fun onCleared() {
        repository.onCleared()
        compositeDisposable.clear()
        super.onCleared()
    }
}