package sk.topo.simplenote.viewmodel

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import io.reactivex.disposables.CompositeDisposable
import sk.topo.simplenote.repository.NotesRepository

/**
 * Project: SimpleNote
 * Created by porac
 * on 14-Feb-19.
 */
class ViewModelFactory : ViewModelProvider.Factory {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return when {
            modelClass.isAssignableFrom(NotesViewModel::class.java) ->
                NotesViewModel(
                    NotesRepository(),
                    CompositeDisposable()
                ) as T

            else -> throw IllegalArgumentException("Unknown ViewModel class")
        }
    }
}