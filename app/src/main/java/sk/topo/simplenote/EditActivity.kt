package sk.topo.simplenote

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.text.Editable
import android.text.TextWatcher
import kotlinx.android.synthetic.main.activity_edit.*
import kotlinx.android.synthetic.main.content_edit.*
import sk.topo.simplenote.api.model.NoteModel

class EditActivity : AppCompatActivity() {

    var id: Int? = null
    var title: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edit)
        setSupportActionBar(toolbar)
        if (intent.hasExtra(EXTRA_NOTE_ID)) {
            id = intent.extras.getInt(EXTRA_NOTE_ID)
        }
        if (intent.hasExtra(EXTRA_NOTE_TITLE)) {
            title = intent.extras.getString(EXTRA_NOTE_TITLE)
        }

        fab.setOnClickListener { view ->
            title = note_edittext.text.toString()
            save()
        }
        note_edittext.addTextChangedListener(object : TextWatcher{
            override fun afterTextChanged(text: Editable?) {
                fab.isEnabled = text.isNullOrBlank().not()
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }
        })
        title?.let {
            note_edittext.setText(it)
            note_edittext.setSelection(it.length)
        }

        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        fab.isEnabled = !title.isNullOrEmpty()
    }

    private fun save() {
        val resultIntent = Intent()
        id?.let {
            resultIntent.putExtra(EXTRA_NOTE_ID, it)
        }
        title?.let {
            resultIntent.putExtra(EXTRA_NOTE_TITLE, it)
        }
        setResult(Activity.RESULT_OK, resultIntent)
        finish()
    }

    companion object {
        val CODE_ADD = 1
        val CODE_EDIT = 2
        const val EXTRA_NOTE_ID = "EXTRA_NOTE_ID"
        const val EXTRA_NOTE_TITLE = "EXTRA_NOTE_TITLE"

        fun startForAdd(activity: AppCompatActivity) {
            activity.startActivityForResult(Intent(activity, EditActivity::class.java), CODE_ADD)
        }

        fun startForEdit(activity: AppCompatActivity, note: NoteModel) {
            val intent = Intent(activity, EditActivity::class.java)
            intent.putExtra(EXTRA_NOTE_ID, note.id)
            intent.putExtra(EXTRA_NOTE_TITLE, note.title)
            activity.startActivityForResult(intent, CODE_EDIT)
        }
    }

}
