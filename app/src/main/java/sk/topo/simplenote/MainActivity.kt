package sk.topo.simplenote

import android.app.Activity
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.content_main.*
import sk.topo.simplenote.api.Resource
import sk.topo.simplenote.api.model.NoteModel
import sk.topo.simplenote.ui.NotesAdapter
import sk.topo.simplenote.util.beGone
import sk.topo.simplenote.util.beVisible
import sk.topo.simplenote.util.beVisibleIf
import sk.topo.simplenote.viewmodel.NotesViewModel
import sk.topo.simplenote.viewmodel.ViewModelFactory

class MainActivity : AppCompatActivity() {

    lateinit var viewModel: NotesViewModel
    lateinit var adapter: NotesAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)
        viewModel = ViewModelProviders.of(this, ViewModelFactory()).get(NotesViewModel::class.java)
        viewModel.notesList.observe(this, Observer {
            when(it) {
                is Resource.Progress -> {
                    main_progressbar.beVisibleIf(it.loading)
                }
                is Resource.Failure -> {
                    main_no_connection_layout.beVisible()
                    main_recycler.beGone()
                    main_title.beGone()
                    Toast.makeText(this, it.e.localizedMessage, Toast.LENGTH_LONG).show()
                }
                is Resource.Success -> {
                    main_no_connection_layout.beGone()
                    it.data.isNotEmpty().run {
                        main_recycler.beVisibleIf(this)
                        main_title.beVisibleIf(this)
                        main_no_content_layout.beVisibleIf(this.not())
                    }
                    adapter.takeData(ArrayList(it.data))
                }
            }
        })
        viewModel.singleNote.observe(this, Observer {
            when(it) {
                is Resource.Progress -> {
                    main_progressbar.beVisibleIf(it.loading)
                }
                is Resource.Failure -> {
                    Snackbar.make(main_recycler, getString(R.string.error_saving_claim), Snackbar.LENGTH_LONG).show()
                    Toast.makeText(this, it.e.localizedMessage, Toast.LENGTH_LONG).show()
                }
                is Resource.Success -> {
                    Snackbar.make(main_recycler, getString(R.string.save_claim), Snackbar.LENGTH_LONG).show()
                    adapter.updateList(it.data)
                }
            }
        })
        viewModel.removingCallback.observe(this, Observer {
            when(it) {
                is Resource.Progress -> {
                    main_progressbar.beVisibleIf(it.loading)
                }
                is Resource.Failure -> {
                    Snackbar.make(main_recycler, getString(R.string.error_saving_claim), Snackbar.LENGTH_LONG).show()
                    Toast.makeText(this, it.e.localizedMessage, Toast.LENGTH_LONG).show()
                }
                is Resource.Success -> {
                    adapter.removeItem(it.data)
                    Snackbar.make(main_recycler, getString(R.string.delete_claim), Snackbar.LENGTH_LONG).show()
                }
            }
        })

        main_no_connection_layout.setOnClickListener {
            viewModel.getNotes()
        }
        fab.setOnClickListener { view ->
            EditActivity.startForAdd(this)

        }
        main_recycler.layoutManager = LinearLayoutManager(this)
        adapter = NotesAdapter(provideListener())
        main_recycler.adapter = adapter
        viewModel.getNotes()
    }

    private fun provideListener(): NotesAdapter.NoteInteractionListener {
        return object: NotesAdapter.NoteInteractionListener {
            override fun removeItem(noteModel: NoteModel) {
                viewModel.deleteNote(noteModel.id)
            }

            override fun onItemClicked(noteModel: NoteModel) {
                EditActivity.startForEdit(this@MainActivity, noteModel)
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (resultCode == Activity.RESULT_OK) {
            when (requestCode) {
                EditActivity.CODE_ADD -> {
                    data?.let {
                        if (it.hasExtra(EditActivity.EXTRA_NOTE_TITLE)) {
                            it.extras.getString(EditActivity.EXTRA_NOTE_TITLE)?.let {title ->
                                viewModel.addNote(title)
                            }
                        }
                    }
                }
                EditActivity.CODE_EDIT -> {
                    data?.let {
                        if (it.hasExtra(EditActivity.EXTRA_NOTE_TITLE) && it.hasExtra(EditActivity.EXTRA_NOTE_ID)) {
                            val title = it.extras.getString(EditActivity.EXTRA_NOTE_TITLE)
                            val id = it.extras.getInt(EditActivity.EXTRA_NOTE_ID)
                            if (title != null) {
                                viewModel.updateNote(id, title)
                            }
                        }
                    }
                }
            }
        }
        super.onActivityResult(requestCode, resultCode, data)
    }

}
