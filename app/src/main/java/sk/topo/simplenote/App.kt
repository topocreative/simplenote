package sk.topo.simplenote

import android.app.Application
import sk.topo.simplenote.api.ApiHandler

/**
 * Project: SimpleNote
 * Created by porac
 * on 14-Feb-19.
 */
class App: Application() {

    override fun onCreate() {
        super.onCreate()
        ApiHandler.init()
    }
}