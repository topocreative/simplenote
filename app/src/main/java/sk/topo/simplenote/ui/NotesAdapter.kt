package sk.topo.simplenote.ui

import android.support.v7.util.DiffUtil
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.TextView
import kotlinx.android.synthetic.main.note_list_item_layout.view.*
import sk.topo.simplenote.R
import sk.topo.simplenote.api.model.NoteModel

/**
 * Project: SimpleNote
 * Created by porac
 * on 14-Feb-19.
 */
class NotesAdapter(val listener: NoteInteractionListener) : RecyclerView.Adapter<NotesAdapter.NoteViewHolder>() {
    private var listItems: ArrayList<NoteModel> = arrayListOf()

    fun takeData(newData: ArrayList<NoteModel>) {
        val diffCallback = NotesDiffutilCallback(this.listItems, newData)
        val diffResult = DiffUtil.calculateDiff(diffCallback)
        this.listItems = newData
        diffResult.dispatchUpdatesTo(this)
    }

    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): NoteViewHolder {
        val view = LayoutInflater.from(viewGroup.context).inflate(R.layout.note_list_item_layout, viewGroup, false)
        return NoteViewHolder(view)
    }

    override fun getItemCount(): Int {
        return listItems.size
    }

    override fun onBindViewHolder(holder: NoteViewHolder, position: Int) {
        listItems.getOrNull(position)?.let {item ->
            holder.title.text = item.title
            holder.itemView.setOnClickListener {
                listener.onItemClicked(item)
            }
            holder.delete_button.setOnClickListener {
                listener.removeItem(item)
            }
        }
    }

    fun updateList(note: NoteModel) {
        val newData = ArrayList(listItems.filter { it.id != note.id })
        newData.add(note)
        takeData(newData)
    }

    fun removeItem(id: Int) {
        val newData = ArrayList(listItems.filter { it.id != id })
        takeData(newData)
    }


    inner class NoteViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val title: TextView = view.note_list_item_title
        val delete_button: ImageButton = view.note_list_item_delete_button
    }

    interface NoteInteractionListener {
        fun onItemClicked(noteModel: NoteModel)
        fun removeItem(noteModel: NoteModel)
    }
}