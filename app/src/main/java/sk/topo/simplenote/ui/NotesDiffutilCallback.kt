package sk.topo.simplenote.ui

import android.support.v7.util.DiffUtil
import sk.topo.simplenote.api.model.NoteModel

/**
 * Project: SimpleNote
 * Created by porac
 * on 14-Feb-19.
 */
class NotesDiffutilCallback(val oldList: List<NoteModel>, val newList: List<NoteModel>) : DiffUtil.Callback() {
    override fun getOldListSize(): Int = oldList.size

    override fun getNewListSize(): Int = newList.size

    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return oldList[oldItemPosition].id == newList[newItemPosition].id
    }

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return oldList[oldItemPosition].title == newList[newItemPosition].title
    }
}