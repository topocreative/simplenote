package sk.topo.simplenote.util

import android.view.View

/**
 * View extensions
 */

fun View.beVisible() {
    visibility = View.VISIBLE
}

fun View.beInvisible() {
    visibility = View.INVISIBLE
}

fun View.beGone() {
    visibility = View.GONE
}

fun View.beVisibleIf(beVisible: Boolean) {
    visibility = if (beVisible)
        View.VISIBLE
    else
        View.GONE
}