package sk.topo.simplenote.api.model

data class NoteModel(
    val id: Int,
    val title: String
)