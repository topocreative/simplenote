/*
 * Copyright (C) 2017 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package sk.topo.simplenote.api

/**
 * A generic class that holds a value with its loading status.
 * @param <T>
</T> */
sealed class Resource<out T> {
    data class Progress<T>(var loading: Boolean) : Resource<T>()
    data class Success<T>(var data: T) : Resource<T>()
    data class Failure<T>(val e: Throwable) : Resource<T>()

    companion object {
        fun <T> loading(isLoading: Boolean): Resource<T> = Progress(isLoading)

        fun <T> success(data: T): Resource<T> = Success(data)

        fun <T> failure(e: Throwable): Resource<T> = Failure(e)
    }
}
