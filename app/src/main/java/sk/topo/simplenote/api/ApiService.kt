package sk.topo.simplenote.api

import io.reactivex.Observable
import retrofit2.Response
import retrofit2.http.*
import sk.topo.simplenote.api.model.NoteModel

/**

 */
interface ApiService {

    @POST("notes")
    fun newNote(@Body title: String): Observable<Response<NoteModel>>

    @PUT("notes/{id}")
    fun updateNote(@Path("id") id: Int, @Body title: String): Observable<Response<NoteModel>>

    @DELETE("notes/{id}")
    fun deleteNote(@Path("id") id: Int): Observable<Response<Any>>

    @GET("notes/{id}")
    fun getNote(@Path("id") id: Int): Observable<Response<NoteModel>>

    @GET("notes")
    fun getNotes(): Observable<Response<List<NoteModel>>>

}