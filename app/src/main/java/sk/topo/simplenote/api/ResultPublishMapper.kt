package sk.topo.simplenote.api

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import io.reactivex.Observable
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.subjects.PublishSubject

/**
 * Extension function to convert a Publish subject into a LiveData by subscribing to it.
 **/
fun <T> Observable<T>.toLiveData(compositeDisposable: CompositeDisposable): LiveData<T> {
    val data = MutableLiveData<T>()
    compositeDisposable.add(this.subscribe({ t: T -> data.value = t }))
    return data
}

/**
 * Extension function to push a failed event with an exception to the observing outcome
 * */
fun <T> PublishSubject<Resource<T>>.failed(e: Throwable) {
    with(this) {
        loading(false)
        onNext(Resource.failure(e))
    }
}

/**
 * Extension function to push  a success event with data to the observing Resource
 * */
fun <T> PublishSubject<Resource<T>>.success(t: T) {
    with(this) {
        loading(false)
        onNext(Resource.success(t))
    }
}

/**
 * Extension function to push the loading status to the observing Resource
 * */
fun <T> PublishSubject<Resource<T>>.loading(isLoading: Boolean) {
    this.onNext(Resource.loading(isLoading))
}
