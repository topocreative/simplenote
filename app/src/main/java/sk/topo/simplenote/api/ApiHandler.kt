package sk.topo.simplenote.api

import com.facebook.stetho.okhttp3.StethoInterceptor
import com.google.gson.FieldNamingPolicy
import com.google.gson.GsonBuilder
import io.reactivex.schedulers.Schedulers
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import sk.topo.simplenote.BuildConfig

/**
 */
object ApiHandler {

    const val API_BASE_URL = "http://private-9aad-note10.apiary-mock.com/"
    private lateinit var retrofit: Retrofit
    val apiService: ApiService by lazy { retrofit.create(ApiService::class.java) }

    fun init() {
        retrofit = provideRetrofit(provideOkHttpClient(), provideGson())
    }


    private fun provideRetrofit(okHttpClient: OkHttpClient, gson: GsonConverterFactory): Retrofit {
        val retrofit = Retrofit.Builder()
            .addCallAdapterFactory(RxJava2CallAdapterFactory.createWithScheduler(Schedulers.io()))
            .addConverterFactory(gson)
            .client(okHttpClient)
            .baseUrl(API_BASE_URL)
            .build()

        return retrofit
    }

    private fun provideOkHttpClient(): OkHttpClient {
        val interceptor = HttpLoggingInterceptor()
        interceptor.level = HttpLoggingInterceptor.Level.BODY
        val addInterceptor = OkHttpClient.Builder()
            .addInterceptor(interceptor)
            //.cache(Cache(File(application.cacheDir, "httpcache"), 10 * 1024 * 1024))
        if (BuildConfig.DEBUG) {
            addInterceptor.addNetworkInterceptor(StethoInterceptor())
        }
        return addInterceptor.build()
    }

    private fun provideGson(): GsonConverterFactory {
        return GsonConverterFactory.create(
            GsonBuilder()
                .disableHtmlEscaping()
                .setFieldNamingPolicy(FieldNamingPolicy.IDENTITY)
                .create()
        )
    }
}