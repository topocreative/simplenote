package sk.topo.simplenote.repository

import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.PublishSubject
import sk.topo.simplenote.api.*
import sk.topo.simplenote.api.model.NoteModel

/**
 * Project: SimpleNote
 * Created by porac
 * on 14-Feb-19.
 */
class NotesRepository {

    val disposable: CompositeDisposable = CompositeDisposable()

    val notesList: PublishSubject<Resource<List<NoteModel>>> =
        PublishSubject.create<Resource<List<NoteModel>>>()

    val singleNote: PublishSubject<Resource<NoteModel>> =
        PublishSubject.create<Resource<NoteModel>>()

    val removingCallback: PublishSubject<Resource<Int>> =
        PublishSubject.create<Resource<Int>>()

    fun getNotes() {
        notesList.loading(true)
        disposable.add(ApiHandler.apiService.getNotes()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                {
                    notesList.loading(false)
                    val noteListData = it.body()
                    if (it.code() == 200 && noteListData != null) {
                        notesList.success(noteListData)
                    } else {
                        notesList.failed(Throwable("Server error"))
                    }
                },
                {
                    notesList.loading(false)
                    notesList.failed(it)
                }
            )
        )
    }

    fun newNote(title: String) {
        singleNote.loading(true)
        disposable.add(ApiHandler.apiService.newNote(title)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                {
                    singleNote.loading(false)
                    val noteData = it.body()
                    if (it.code() == 201 && noteData != null) {
                        singleNote.success(noteData)
                    } else {
                        singleNote.failed(Throwable("Server error"))
                    }
                },
                {
                    singleNote.loading(false)
                    singleNote.failed(it)
                }
            )
        )
    }

    fun getNote(id: Int) {
        singleNote.loading(true)
        disposable.add(ApiHandler.apiService.getNote(id)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                {
                    singleNote.loading(false)
                    val noteData = it.body()
                    if (it.code() == 200 && noteData != null) {
                        singleNote.success(noteData)
                    } else {
                        singleNote.failed(Throwable("Server error"))
                    }
                },
                {
                    singleNote.loading(false)
                    singleNote.failed(it)
                }
            )
        )
    }

    fun updateNote(id: Int, title: String) {
        singleNote.loading(true)
        disposable.add(ApiHandler.apiService.updateNote(id, title)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                {
                    singleNote.loading(false)
                    val noteData = it.body()
                    if (it.code() == 201 && noteData != null) {
                        singleNote.success(noteData)
                    } else {
                        singleNote.failed(Throwable("Server error"))
                    }
                },
                {
                    singleNote.loading(false)
                    singleNote.failed(it)
                }
            )
        )
    }

    fun deleteNote(id: Int) {
        removingCallback.loading(true)
        disposable.add(ApiHandler.apiService.deleteNote(id)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                {
                    removingCallback.loading(false)
                    if (it.code() == 204) {
                        removingCallback.success(id)
                    } else {
                        removingCallback.failed(Throwable("Server error"))
                    }
                },
                {
                    removingCallback.loading(false)
                    removingCallback.failed(it)
                }
            )
        )
    }

    fun onCleared() {
        disposable.clear()
    }


}